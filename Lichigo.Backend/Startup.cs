﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Lichigo.Backend.Startup))]
namespace Lichigo.Backend
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

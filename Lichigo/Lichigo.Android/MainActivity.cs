﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Firebase;

namespace Lichigo.Droid
{
    [Activity(Label = "Lichigo", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        public static FirebaseApp app;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            InitFirebaseAuth();

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            FontManager.Current
                .RegisterTypeFace("Warnes/Warnes-Regular.ttf")
                .RegisterTypeFace("Roboto/Roboto-Black.ttf")
                .RegisterTypeFace("Roboto/Roboto-BlackItalic.ttf")
                .RegisterTypeFace("Roboto/Roboto-Medium.ttf")
                .RegisterTypeFace("Roboto/Roboto-BoldItalic.ttf")
                .RegisterTypeFace("Glegoo/Glegoo-Bold.ttf")
                .RegisterTypeFace("Roboto/Glegoo-Regular.ttf")
                .RegisterTypeFace("Roboto/Roboto-Italic.ttf");


            LoadApplication(new App());


        }

        private void InitFirebaseAuth()
        {
            var options = new FirebaseOptions.Builder()
            .SetApplicationId("1:649142855557:android:46afbd6b7ce3fb75")
            .SetApiKey("AIzaSyDSf8kZ4L4N1ItJ33q8TqayUmINYCrXzlI")
            .Build();

            if (app == null)
                app = FirebaseApp.InitializeApp(this, options, "FirebaseSample");
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lichigo.Infrastructure
{
    using ViewModels;

    class InstanceLocator
    {


        #region Properties
        public MainViewModels Main
        {
            get;

            set;
        }
        #endregion

        #region Constructors

        public InstanceLocator()
        {

            this.Main = new MainViewModels();
        }

        #endregion

    }
}

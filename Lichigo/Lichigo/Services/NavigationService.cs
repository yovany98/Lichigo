﻿namespace Lichigo.Services
{

    using System;
    using System.Collections.Generic;
    using System.Text;
    using System.Threading.Tasks;
    using Views;
    using Xamarin.Forms;

    public class NavigationService
    {

        public void SetMainPage( string pageName)
        {
            switch (pageName)
            {
                case "LoginPage":
                    Application.Current.MainPage = new NavigationPage(new LoginPage());
                    break;
                case "MasterView":
                    Application.Current.MainPage = new MasterView();
                    break;
            }
        }

        public async Task NavigateOnMaster(string pageName)
        {

            App.Master.IsPresented = false;

            switch (pageName)
            {
                case "ProductoTabbePage":
                    await App.Navigator.PushAsync(
                        new ProductoTabbePage());
                    break;

                case "LichigoPages":
                    await App.Navigator.PushAsync(
                        new LichigoPages());
                    break;

                case "CategoriasPages":
                    await App.Navigator.PushAsync(
                        new CategoriasPages());
                    break;

                case "ComparaPages":
                    await App.Navigator.PushAsync(
                        new ComparaPages());
                    break;

                case "ProductosPages":
                    await App.Navigator.PushAsync(
                        new ProductosPages());
                    break;
            }


        }

        public async Task NavigateOnLogin(string pageName)
        {
            switch (pageName)
            {
                case "UserFormView":
                    await Application.Current.MainPage.Navigation.PushAsync(
                        new UserFormView());
                    break;

                case "GrocerFormView":
                    await Application.Current.MainPage.Navigation.PushAsync(
                        new GrocerFormView());
                    break;

            }
        }

        public async Task BackOnMaster()
        {
            await App.Navigator.PopAsync();
        }

        public async Task BackOnLogin()
        {
            await Application.Current.MainPage.Navigation.PopAsync();
        }

    }
}

﻿

namespace Lichigo.ViewModels
{
    using GalaSoft.MvvmLight.Command;
    using Views;
    using System.Windows.Input;
    using Xamarin.Forms;
    using Lichigo.Services;
    using System.Threading.Tasks;
     
    public class LoginViewModels : BaseViewModels
    {

        #region Service

        private ApiService apiService;

        #endregion

        #region Attributes

        public string email;
        public string password;
        private bool isRunning;
        private bool isEnabled;
        private ICommand _loginCommand;

        #endregion


        #region Properties

        public string Email
        {
            get { return this.email; }
            set { SetValue(ref this.email, value); }
        }

        public string Password
        {
            get { return this.password; }
            set { SetValue(ref this.password, value); }
        }

        public bool IsRunning
        {
            get { return this.isRunning; }
            set { SetValue(ref this.isRunning, value); }
        }

        public bool IsRememberd
        {
            get;
            set;
        }

        public bool IsEnabled
        {
            get { return this.isEnabled; }
            set { SetValue(ref this.isEnabled, value); }
        }

        #endregion

        #region Constructor

        public LoginViewModels()
        {
            this.apiService = new ApiService();

            this.IsRememberd = true;
            this.IsEnabled = true;
        }

        #endregion

        #region Commands


        public ICommand RegistraseUserComand
        {
            get
            {
                return new RelayCommand(UserForm);
            }
        }

        private async void UserForm()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new UserFormView());


            await Application.Current.MainPage.DisplayAlert(

                   "Hola",
                   "Bienvenidos a nuestra comunidad Lichigo ",
                   "Aceptar");
            return;
        }

        public ICommand ResgistrarTenderoCommand
        {
            get
            {
                return new RelayCommand(GrocerForm);
            }
        }

        private async void GrocerForm()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new GrocerFormView());


            await Application.Current.MainPage.DisplayAlert(

                   "Hola",
                   "Bienvenidos a nuestra comunidad Lichigo tenderos ",
                   "Aceptar");
            return;
        }


        private async Task LoginCommandExecute()
        {
            var connection = await this.apiService.CheckConnection();

            if (!connection.IsSuccess)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                "Error",
                connection.Message,
                "Aceptar");
                return;
            }

            var token = await this.apiService.GetToken(
             "https://lichigoapi1.azurewebsites.net",
             this.Email,
             this.Password);

            if(token == null)
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                "Error",
                "Algo anda mal, Por favor intente mas tarde ",
                "Aceptar");
                return;
            }

            if (string.IsNullOrEmpty(token.AccessToken))
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                await Application.Current.MainPage.DisplayAlert(
                "Error",
                token.ErrorDescription,
                "Aceptar");

                this.Password = string.Empty;

                return;
            }

            else
            {
                this.IsRunning = false;
                this.IsEnabled = true;
                if (string.IsNullOrEmpty(this.Email))
                {
                    this.IsRunning = false;
                    this.IsEnabled = true;
                    await Application.Current.MainPage.DisplayAlert(
                "Error",
                "Ingrese un correo electronico",
                "Aceptar");
                    return;
                }

                else if (string.IsNullOrEmpty(this.Password))
                {
                    this.IsRunning = false;
                    this.IsEnabled = true;

                    await Application.Current.MainPage.DisplayAlert(
                "Error",
                "Ingrese una contraseña ",
                "Aceptar");

                    return;
                }

                else
                {
                    this.IsRunning = false;
                    this.IsEnabled = true;

                    await Application.Current.MainPage.DisplayAlert(
                "Error",
                "El correo o la contraseña esta mal escritas",
                "Aceptar");

                    this.Password = string.Empty;
                    return;
                }

            }

            var mainViewModels = MainViewModels.GetInstance();
            mainViewModels.Token = token;
            mainViewModels.Lichigo = new LichigoPagesViewModels();
            await Application.Current.MainPage.Navigation.PushAsync(new LichigoPages());

            this.IsRunning = false;
            this.IsEnabled = true;

            this.Email = string.Empty;
            this.Password = string.Empty;


        }

        #endregion



    }
}

﻿
namespace Lichigo.ViewModels
{
    using System.Collections.ObjectModel;
    using Models;
    using Menu = Models.Menu;

    public class MainViewModels
    {

        #region Propeties

        public ObservableCollection<Menu> MyMenu
        {
            get;
            set;
        }

        public TokenResponse Token
        {
            get;
            set;
        }

        #endregion

        #region ViewModels

        public LoginViewModels Login
        {
            get;
            set;
        }

        public LichigoPagesViewModels Lichigo
        {
            get;
            set;
        }

        public UserFormViewModels UserForm
        {
            get;
            set;
        }

        public GrocerFormViewModels GrocerForm
        {
            get;
            set;
        }
        #endregion

        #region Singleton

        private static MainViewModels instance;

        public  static MainViewModels GetInstance()
        {

            if (instance == null)
            {

                return new MainViewModels();

            }

            return instance;
        }

        #endregion

        #region Constructor

        public MainViewModels()
        {

            instance = this;
            this.Login = new LoginViewModels();
            LoadMenu();
        }

        #endregion

        #region Methods

        private void LoadMenu()
        {
            MyMenu = new ObservableCollection<Menu>();

            MyMenu.Add(new Menu
            {

                Icon = "Bag_48px.png",
                PageName="ComparaView",
                Title="Compara",
            });


            MyMenu.Add(new Menu
            {

                Icon = "Settings_48px.png",
                PageName = "ConfiguracionesView",
                Title = "Configuraciones",
            });

            MyMenu.Add(new Menu
            {

                Icon = "Running_48px.png",
                PageName = "LoginPage",
                Title = "Cerrar Sesion",
            });
        }

        #endregion
    }
}
